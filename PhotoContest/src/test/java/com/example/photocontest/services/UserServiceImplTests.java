package com.example.photocontest.services;

import com.example.photocontest.Helpers;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Rank;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;
import com.example.photocontest.repositories.contracts.RoleRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.example.photocontest.Helpers.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void get_Should_CallRepository() {
        // Arrange
        UserFilterOptions mockFilterOptions = createMockUserFilterOptions();

        Mockito.when(mockRepository.get(mockFilterOptions))
                .thenReturn(List.of());

        // Act
        userService.get(mockFilterOptions);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .get(mockFilterOptions);
    }

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(List.of());

        // Act
        userService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void get_Should_Return_User_When_MatchByIdExist() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        Mockito.when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        User result = userService.get(mockUser.getId());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }


    @Test
    void get_Should_Return_User_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        Mockito.when(mockRepository.get(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        User result = userService.get(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    void create_Should_Throw_When_UserWithSameUsernameExist() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        // Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> userService.create(mockUser)
        );
    }

    @Test
    void create_Should_CallRepository_When_UserNotExist() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        Mockito.when(mockRepository.get(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        userService.create(mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    void update_Should_CallRepository_When_UserExist() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        Mockito.when(mockRepository.get(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        userService.update(mockUser.getId(), mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void isOrganizer_Should_ReturnTrue_When_UserIsOrganizer() {
        // Arrange
        User mockUser = createMockOrganizer();

        // Act
        boolean result = userService.isOrganizer(mockUser);

        // Assert
        Assertions.assertTrue(result);
    }

    @Test
    void isOrganizer_Should_ReturnFalse_When_UserIsNotOrganizer() {
        // Arrange
        User mockUser = createMockPhotoJunkie();

        // Act
        boolean result = userService.isOrganizer(mockUser);

        // Assert
        Assertions.assertFalse(result);
    }

    @Test
    void getUsersByRank_Should_CallRepository() {
        // Arrange
        User mockUser = Helpers.createMockOrganizer();
        Rank mockRank = Helpers.createMockRankJunkie();

        Mockito.when(mockRepository.getUsersByRank(mockUser, mockRank.getName()))
                .thenReturn(List.of());

        // Act
        userService.getUsersByRank(mockUser, mockRank.getName());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getUsersByRank(mockUser, mockRank.getName());
    }

    @Test
    void getUserByRank_Should_Throw_When_UserIsNotOrganizer() {
        // Arrange
        User mockUser = Helpers.createMockPhotoJunkie();
        Rank mockRank = Helpers.createMockRankJunkie();

        String rankName = mockRank.getName();
        // Act
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> userService.getUsersByRank(mockUser, rankName)
        );
    }

    @Test
    void changeRoleToUser_Should_CallRepository_When_UserIsOrganizer() {
        // Arrange
        User mockUser = Helpers.createMockOrganizer();
        User mockUser2 = Helpers.createMockPhotoJunkie();

        Mockito.when(mockRepository.get(mockUser2.getId()))
                .thenReturn(mockUser2);

        Mockito.when(mockRoleRepository.get(Mockito.anyString()))
                .thenReturn(createMockRolePhotoJunkie());

        // Act
        userService.changeRoleOfUser(mockUser, mockUser2.getId());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUser);
    }

    @Test
    void changeRoleUser_Should_Throw_When_UserIsNotOrganizer() {
        // Arrange
        User mockUser = Helpers.createMockPhotoJunkie();
        User mockUser2 = Helpers.createMockPhotoJunkie();

        int id2 = mockUser2.getId();

        // Act
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> userService.changeRoleOfUser(mockUser, id2)
        );
    }

    /*@Test
    void getUserByUsername_Should_CallRepository(){
        // Arrange
       *//* User mockUser = createMockPhotoJunkie();*//*

        Mockito.when(mockRepository.get(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        userService.getUserByUsername(mockUser.getUsername());

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .get(mockUser.getUsername());
    }*/
}
