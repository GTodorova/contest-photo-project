package com.example.photocontest.services;

import com.example.photocontest.Helpers;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestRepository mockRepository;
    @Mock
    UserRepository mockUserRepository;
    @InjectMocks
    ContestServiceImpl contestService;

    @Test
    public void get_Should_CallRepository_And_ReturnList() {
        // Arrange
        ContestFilterOptions contestFilterOptions = new ContestFilterOptions();
        List<Contest> expectedContests = new ArrayList<>();
        expectedContests.add(new Contest());
        when(mockRepository.get(contestFilterOptions))
                .thenReturn(expectedContests);

        // Act
        List<Contest> actualContests = contestService.get(contestFilterOptions);

        // Assert
        verify(mockRepository, times(1)).get(contestFilterOptions);
        assertEquals(expectedContests, actualContests);
    }

    @Test
    public void getAllUsers_Should_CallRepository() {
        // Arrange
        when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        contestService.getAll();
        // Assert
        verify(mockRepository, times(1)).getAll();
    }

    @Test
    void getContestJuryByContestId_Should_ReturnCorrectJury() {
        // Arrange
        Contest mockContest = Helpers.createMockContestNotInvitational();
        Set<User> mockJuryMembers = Helpers.createMockJuryMembers();
        mockContest.setJuryMembers(mockJuryMembers);
        User mockOrganizer = Helpers.createMockOrganizer();
        mockContest.setOrganizer(mockOrganizer);
        List<User> mockOrganizers = Helpers.createMockOrganizers();
        when(mockRepository.getById(mockContest.getId())).thenReturn(mockContest);
        when(mockUserRepository.getOrganizers()).thenReturn(mockOrganizers);

        // Act
        List<User> result = contestService.getContestJuryByContestId(mockContest.getId());

        // Assert
        assertEquals(mockJuryMembers.size() + mockOrganizers.size() - 1, result.size()); // -1 to exclude the organizer
        assertTrue(result.containsAll(mockJuryMembers));
        assertTrue(result.containsAll(mockOrganizers));
    }

    @Test
    void checkContestPhaseType_ShouldReturnFirst_WhenPhaseOneActive() {
        // Arrange
        Contest contest = new Contest();
        contest.setPhaseOneStart(LocalDateTime.now().minusDays(1));
        contest.setPhaseOneEnd(LocalDateTime.now().plusDays(1));
        contest.setPhaseTwoStart(LocalDateTime.now().plusDays(2));
        contest.setPhaseTwoEnd(LocalDateTime.now().plusDays(3));

        // Act
        String phaseType = contestService.checkContestPhaseType(contest);

        // Assert
        assertEquals("First", phaseType);
    }

    @Test
    void checkContestPhaseType_ShouldReturnSecond_WhenPhaseTwoActive() {
        // Arrange
        Contest contest = new Contest();
        contest.setPhaseOneStart(LocalDateTime.now().minusDays(3));
        contest.setPhaseOneEnd(LocalDateTime.now().minusDays(2));
        contest.setPhaseTwoStart(LocalDateTime.now().minusDays(1));
        contest.setPhaseTwoEnd(LocalDateTime.now().plusDays(1));

        // Act
        String phaseType = contestService.checkContestPhaseType(contest);

        // Assert
        assertEquals("Second", phaseType);
    }

    @Test
    void checkContestPhaseType_ShouldReturnFinished_WhenContestEnded() {
        // Arrange
        Contest contest = new Contest();
        contest.setPhaseOneStart(LocalDateTime.now().minusDays(5));
        contest.setPhaseOneEnd(LocalDateTime.now().minusDays(4));
        contest.setPhaseTwoStart(LocalDateTime.now().minusDays(3));
        contest.setPhaseTwoEnd(LocalDateTime.now().minusDays(2));

        // Act
        String phaseType = contestService.checkContestPhaseType(contest);

        // Assert
        assertEquals("Finished", phaseType);
    }

    @Test
    void checkIfOrganizerIsInvited_Should_Return_True_When_Organizer_Is_Invited() {
        // Arrange
        Set<User> organizers = new HashSet<>();
        Set<User> invitedUsers = new HashSet<>();
        User organizer = new User();
        organizer.setId(1);
        organizers.add(organizer);
        invitedUsers.add(organizer);

        // Act
        boolean result = contestService.checkIfOrganizerIsInvited(organizers, invitedUsers);

        // Assert
        assertTrue(result);
    }

    @Test
    void checkIfOrganizerIsInvited_Should_Return_False_When_Organizer_Is_Not_Invited() {
        // Arrange
        Set<User> organizers = new HashSet<>();
        Set<User> invitedUsers = new HashSet<>();
        User organizer = new User();
        organizer.setId(1);
        organizers.add(organizer);
        User invitedUser = new User();
        invitedUser.setId(2);
        invitedUsers.add(invitedUser);

        // Act
        boolean result = contestService.checkIfOrganizerIsInvited(organizers, invitedUsers);

        // Assert
        assertFalse(result);
    }

    @Test
    void checkIfJuryIsInvited_ShouldReturnTrue_WhenJuryIsInvited() {
        // Arrange
        Set<User> invitedUsers1 = Helpers.createMockInvitedUsers();
        Set<User> jury = Helpers.createMockJuryMembers();
        // Act
        boolean result = contestService.checkIfJuryIsInvited(jury, invitedUsers1);
        // Assert
        assertTrue(result);
    }

    @Test
    void getContestPhaseOne_ReturnsCorrectContests_WhenCalledWithValidArguments() {
        // Arrange
        LocalDateTime currentDate = LocalDateTime.now();
        User organizer = Helpers.createMockOrganizer();
        List<Contest> contests = new ArrayList<>();
        Contest contest3 = new Contest();
        contest3.setPhaseOneStart(currentDate.plusDays(1));
        contest3.setPhaseOneEnd(currentDate.plusDays(2));
        contests.add(contest3);
        Mockito.when(mockRepository.getContestPhaseOne(currentDate)).thenReturn(contests);

        // Act
        List<Contest> result = contestService.getContestPhaseOne(currentDate, organizer);

        // Assert
        assertEquals(1, result.size());
        assertEquals(contest3, result.get(0));
    }

    @Test
    void getContestPhaseTwo_ShouldReturnContests_WhenInvokedByOrganizerInPhaseTwo() {
        // Arrange
        User organizer = Helpers.createMockOrganizer();
        Contest contest = Helpers.createMockContestNotInvitational();
        when(mockRepository.getContestPhaseTwo(any())).thenReturn(List.of(contest));

        // Act
        List<Contest> contests = contestService.getContestPhaseTwo(LocalDateTime.now(), organizer);

        // Assert
        assertEquals(1, contests.size());
        assertEquals(contest, contests.get(0));
        verify(mockRepository, times(1)).getContestPhaseTwo(any());
    }

    @Test
    public void getMoreRecentContest_Should_return_More_Recent() {

        //Arrange
        Contest mockContest = Helpers.createMockContestNotInvitational();
        Contest mockContestOther = Helpers.createMockContestInvitational();

        // Act
        List<Contest> unorderedContests = Arrays.asList(mockContestOther, mockContest);

        Mockito.when(mockRepository.getMoreRecentContests()).thenReturn(unorderedContests);

        List<Contest> result = contestService.getMoreRecentContests();

        List<Contest> expected = Arrays.asList(mockContestOther, mockContest);

        // Assert
        Assertions.assertEquals(expected, result);
    }

}
