package com.example.photocontest.services;

import com.example.photocontest.models.Rank;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.RankRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.photocontest.Helpers.createMockPhotoJunkie;
import static com.example.photocontest.Helpers.createMockRankEnthusiast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RankServiceImplTests {

    @Mock
    RankRepository mockRepository;

    @InjectMocks
    RankServiceImpl rankService;

    @Test
    void get_Should_ReturnRank_When_MatchByIdExist() {
        // Arrange
        Rank mockRank = createMockRankEnthusiast();

        when(mockRepository.get(Mockito.anyInt()))
                .thenReturn(mockRank);

        // Act
        Rank result = rankService.get(mockRank.getId());

        // Assert
        assertEquals(mockRank, result);
    }

    @Test
    void get_Should_ReturnRank_When_MatchByNameExist() {
        // Arrange
        Rank mockRank = createMockRankEnthusiast();

        when(mockRepository.get(Mockito.anyString()))
                .thenReturn(mockRank);

        // Act
        Rank result = rankService.get(mockRank.getName());

        // Assert
        assertEquals(mockRank, result);
    }

    @Test
    void getRankByScore_Should_ReturnRank_When_MatchByScoreExist() {
        // Arrange
        Rank mockRank = createMockRankEnthusiast();

        int minScore = mockRank.getMinScore();

        when(mockRepository.getRankByScore(Mockito.anyInt()))
                .thenReturn(mockRank);

        // Act
        Rank result = rankService.getRankByScore(minScore);

        // Assert
        assertEquals(mockRank, result);
    }

    @Test
    void assignsRankToUser_Should_AssignRankToUser_When_UserHasReachedMinScore() {
        Rank mockRank = createMockRankEnthusiast();

        Mockito.when(mockRepository.getRankByScore(51))
                .thenReturn(mockRank);

        User user = createMockPhotoJunkie();

        rankService.assignRank(51, user);

        assertEquals(mockRank, user.getRank());
    }

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        // Act
        rankService.getAll();
        // Assert
        verify(mockRepository, times(1)).getAll();
    }

}
