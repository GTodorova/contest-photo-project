package com.example.photocontest;

import com.example.photocontest.models.*;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.photocontest.utils.UserRankConstants.*;
import static com.example.photocontest.utils.UserRoleConstants.ORGANIZER;
import static com.example.photocontest.utils.UserRoleConstants.PHOTO_JUNKIE;

public class Helpers {

    public static final String MOCK_CONTEST_TITLE = "MockTitle";
    public static final String MOCK_COVER_PHOTO_NAME = "mockCoverPhotoName";
    public static final String MOCK_CATEGORY_NAME = "mockCategory";
    public static final String MOCK_CATEGORY_NAME_TWO = "mockCategoryTwo";
    public static final String RANK_MASTER = "Master";

    public static User createMockOrganizer() {
        User mockUser = createMockUser();
        Role roleOrganizer = createMockRoleOrganizer();
        Rank rankOrganizer = createMockRankEnthusiast();
        mockUser.setRole(roleOrganizer);
        mockUser.setRank(rankOrganizer);
        mockUser.setScore(100);
        return mockUser;
    }

    public static User createMockPhotoJunkie() {
        User mockUser = createMockUser();
        Role rolePhotoJunkie = createMockRolePhotoJunkie();
        Rank rankPhotoJunkie = createMockRankJunkie();
        mockUser.setRole(rolePhotoJunkie);
        mockUser.setRank(rankPhotoJunkie);
        mockUser.setScore(10);
        return mockUser;
    }

    public static User createMockMaster() {
        User mockUser = createMockUser();
        Role rolePhotoJunkie = createMockRolePhotoJunkie();
        Rank rankMaster = createMockRankMaster();
        mockUser.setRole(rolePhotoJunkie);
        mockUser.setRank(rankMaster);
        mockUser.setScore(160);
        return mockUser;
    }

    public static Set<User> createMockJuryMembers() {
        Set<User> jury = new HashSet<>();
        jury.add(createMockMaster());
        return jury;
    }

    public static List<User> createMockOrganizers() {
        List<User> organizers = new ArrayList<>();
        organizers.add(createMockJurorOrganizer());
        return organizers;
    }

    public static Set<User> createMockInvitedUsers() {
        Set<User> invitedUsers = new HashSet<>();
        invitedUsers.add(createMockPhotoJunkie());
        return invitedUsers;
    }


    public static Contest createMockContestNotInvitational() {
        Contest mockContest = new Contest();
        mockContest.setTitle(MOCK_CONTEST_TITLE);
        Category mockCategory = createMockCategory();
        mockContest.setCategory(mockCategory);
        Set<User> juryMembers = new HashSet<>();
        juryMembers.add(createMockJurorPhotoJunkie());
        mockContest.setJuryMembers(juryMembers);
        mockContest.setInvitational(false);
        User mockOrganizer = createMockOrganizer();
        mockContest.setOrganizer(mockOrganizer);
        mockContest.setCreatedAt(LocalDateTime.now());
        mockContest.setPhaseOneStart(LocalDateTime.now());
        mockContest.setPhaseTwoStart(LocalDateTime.now().plusDays(15));
        mockContest.setPhaseOneEnd(LocalDateTime.now().plusDays(15));
        mockContest.setPhaseTwoEnd(LocalDateTime.now().plusDays(15).plusHours(10));
        mockContest.setCoverPhotoName(MOCK_COVER_PHOTO_NAME);
        mockContest.setId(1);
        return mockContest;
    }

    public static Contest createMockContestNotInvitationalFinished() {
        Contest mockContest = new Contest();
        mockContest.setTitle(MOCK_CONTEST_TITLE);
        Category mockCategory = createMockCategory();
        mockContest.setCategory(mockCategory);
        Set<User> juryMembers = new HashSet<>();
        juryMembers.add(createMockJurorPhotoJunkie());
        mockContest.setJuryMembers(juryMembers);
        mockContest.setInvitational(false);
        User mockOrganizer = createMockOrganizer();
        mockContest.setOrganizer(mockOrganizer);
        mockContest.setCreatedAt(LocalDateTime.now().minusDays(10));
        mockContest.setPhaseOneStart(LocalDateTime.now().minusDays(10));
        mockContest.setPhaseTwoStart(LocalDateTime.now().minusDays(5));
        mockContest.setPhaseOneEnd(LocalDateTime.now().minusDays(5));
        mockContest.setPhaseTwoEnd(LocalDateTime.now().minusDays(5).plusHours(10));
        mockContest.setCoverPhotoName(MOCK_COVER_PHOTO_NAME);
        mockContest.setId(1);
        return mockContest;
    }

    public static Contest createMockContestInvitational() {
        Contest mockContest = new Contest();
        mockContest.setTitle(MOCK_CONTEST_TITLE);
        Category mockCategory = createMockCategory();
        mockContest.setCategory(mockCategory);
        mockContest.setInvitational(true);
        User mockOrganizer = createMockOrganizer();
        mockContest.setOrganizer(mockOrganizer);
        mockContest.setCreatedAt(LocalDateTime.now());
        mockContest.setPhaseOneStart(LocalDateTime.now());
        mockContest.setPhaseTwoStart(LocalDateTime.now().plusDays(15));
        mockContest.setPhaseOneEnd(LocalDateTime.now().plusDays(15));
        mockContest.setPhaseTwoEnd(LocalDateTime.now().plusDays(15).plusHours(10));
        mockContest.setCoverPhotoName(MOCK_COVER_PHOTO_NAME);
        mockContest.setId(2);
        Set<User> invitedUsers = new HashSet<>();
        invitedUsers.add(createMockPhotoJunkie());
        mockContest.setInvitedUsers(invitedUsers);
        return mockContest;
    }


    private static User createMockUser() {
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword");
        mockUser.setCreatedAt(LocalDateTime.now());
        mockUser.setInactive(false);
        return mockUser;
    }

    public static UserFilterOptions createMockUserFilterOptions() {
        return new UserFilterOptions(
                "MockUsername",
                "MockFirstName",
                "MockLastName",
                "sort",
                "order");
    }

    public static Rank createMockRankJunkie() {
        Rank rank = new Rank();
        rank.setId(1);
        rank.setName(INITIAL_RANK);
        return rank;
    }

    public static Rank createMockRankEnthusiast() {
        Rank rank = new Rank();
        rank.setId(2);
        rank.setName(ENTHUSIAST);
        rank.setMinScore(51);
        rank.setMaxScore(150);
        return rank;
    }

    public static Rank createMockRankMaster() {
        Rank rank = new Rank();
        rank.setId(3);
        rank.setName(RANK_MASTER);
        return rank;
    }


    public static Category createMockCategory() {
        Category category = new Category();
        category.setId(1);
        category.setName(MOCK_CATEGORY_NAME);
        return category;
    }

    public static Category createMockOtherCategory() {
        Category category = new Category();
        category.setId(2);
        category.setName(MOCK_CATEGORY_NAME_TWO);
        return category;
    }

    public static Rank createMockRankPhotoDictator() {
        Rank rank = new Rank();
        rank.setId(3);
        rank.setName(WISE_AND_BENEVOLENT_PHOTO_DICTATOR);
        return rank;
    }


    private static Role createMockRoleOrganizer() {
        Role role = new Role();
        role.setId(1);
        role.setName(ORGANIZER);
        return role;
    }

    public static Role createMockRolePhotoJunkie() {
        Role role = new Role();
        role.setId(2);
        role.setName(PHOTO_JUNKIE);
        return role;
    }


    public static ContestPhoto createMockContestPhoto() {
        ContestPhoto contestPhoto = new ContestPhoto();
        contestPhoto.setId(1);
        contestPhoto.setContest(createMockContestNotInvitational());
        contestPhoto.setUser(createMockPhotoJunkie());
        contestPhoto.setPhotoUrl("/mock/photo/url");
        contestPhoto.setTitle("MockTitle");
        contestPhoto.setStory("MockStory");
        contestPhoto.setCreatedAt(LocalDateTime.now());
        return contestPhoto;
    }

    public static ContestPhotoFilterOptions createMockContestPhotoFilterOptions() {
        return new ContestPhotoFilterOptions(
                "MockTitle",
                "sort",
                "order");
    }

    public static PhotoReview createMockPhotoReviewByOrganizer() {
        PhotoReview photoReview = new PhotoReview();
        photoReview.setId(1);
        photoReview.setContestPhoto(createMockContestPhoto());
        photoReview.setJuryMember(createMockJurorOrganizer());
        photoReview.setComment("MockComment");
        photoReview.setWrongCategory(false);
        photoReview.setScore(6);
        photoReview.setContest(createMockContestNotInvitational());
        return photoReview;
    }

    public static User createMockJurorOrganizer() {
        User mockUser = createMockUser();
        Role roleJuror = createMockRoleOrganizer();
        Rank rankJuror = createMockRankEnthusiast();
        mockUser.setRole(roleJuror);
        mockUser.setRank(rankJuror);
        mockUser.setScore(1124);
        return mockUser;
    }

    public static PhotoReview createMockPhotoReviewByPhotoJunkie() {
        PhotoReview photoReview = new PhotoReview();
        photoReview.setId(2);
        photoReview.setContestPhoto(createMockContestPhoto());
        photoReview.setJuryMember(createMockJurorPhotoJunkie());
        photoReview.setComment("MockComment");
        photoReview.setWrongCategory(false);
        photoReview.setScore(6);
        photoReview.setContest(createMockContestNotInvitational());
        return photoReview;
    }

    public static User createMockJurorPhotoJunkie() {
        User mockUser = new User();
        mockUser.setId(2);
        mockUser.setFirstName("MockFirstNameJurorJunkie");
        mockUser.setLastName("MockLastNameJurorJunkie");
        mockUser.setEmail("mock@userJurorJunkie.com");
        mockUser.setUsername("MockUsernameJurorJunkie");
        mockUser.setPassword("MockPasswordJurorJunkie");
        mockUser.setCreatedAt(LocalDateTime.now());
        mockUser.setInactive(false);
        Role roleJuror = createMockRolePhotoJunkie();
        Rank rankJuror = createMockRankPhotoDictator();
        mockUser.setRole(roleJuror);
        mockUser.setRank(rankJuror);
        mockUser.setScore(1400);
        return mockUser;
    }

    public static PhotoReview createMockInvalidPhotoReview() {
        PhotoReview photoReview = new PhotoReview();
        photoReview.setId(2);
        photoReview.setContestPhoto(createMockContestPhoto());
        photoReview.setJuryMember(createMockJurorPhotoJunkie());
        photoReview.setComment("The photo is not in the correct category");
        photoReview.setWrongCategory(true);
        photoReview.setScore(0);
        photoReview.setContest(createMockContestNotInvitational());
        return photoReview;
    }

    public static WinnerPhoto createMockWinnerPhoto() {
        WinnerPhoto winnerPhoto = new WinnerPhoto();
        winnerPhoto.setPlace(createMockPlace());
        winnerPhoto.setContestPhoto(createMockContestPhoto());
        winnerPhoto.setRatingPoints(50);
        winnerPhoto.setId(1);
        return winnerPhoto;
    }

    public static Place createMockPlace() {
        Place place = new Place();
        place.setId(1);
        place.setPlaceName("Golden");
        return place;
    }

    public static ContestPhotoAndPointsDto createMockContestPhotoAndPointsDto() {
        ContestPhotoAndPointsDto contestPhotoAndPointsDto = new ContestPhotoAndPointsDto(createMockContestPhoto(), 6.00);
        return contestPhotoAndPointsDto;
    }

}
