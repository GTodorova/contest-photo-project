package com.example.photocontest.utils;

public final class UserRankConstants {

    public static final String INITIAL_RANK = "Junkie";
    public static final String ENTHUSIAST = "Enthusiast";
    public static final String MASTER = "Master";
    public static final String WISE_AND_BENEVOLENT_PHOTO_DICTATOR = "Photo Dictator";

    private UserRankConstants() {
    }

}
