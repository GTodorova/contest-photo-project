package com.example.photocontest.helpers.mappers;

import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;
import com.example.photocontest.models.dto.PhotoReviewRequestDto;
import com.example.photocontest.models.dto.PhotoReviewResponseDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.PhotoReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PhotoReviewMapper {

    private final PhotoReviewService photoReviewService;

    private final ContestService contestService;

    private final ContestPhotoService contestPhotoService;


    @Autowired
    public PhotoReviewMapper(PhotoReviewService photoReviewService, ContestService contestService,
                             ContestPhotoService contestPhotoService) {
        this.photoReviewService = photoReviewService;
        this.contestService = contestService;
        this.contestPhotoService = contestPhotoService;
    }

    public PhotoReview fromCreateDto(PhotoReviewRequestDto photoReviewRequestDto, User juror) {
        PhotoReview photoReview = new PhotoReview();
        photoReview.setContest(contestService.getById(photoReviewRequestDto.getContestId()));
        photoReview.setContestPhoto(contestPhotoService.get(photoReviewRequestDto.getContestPhotoId()));
        photoReview.setJuryMember(juror);
        photoReview.setWrongCategory(photoReviewRequestDto.isWrongCategory());
        photoReview.setComment(photoReviewRequestDto.getComment());
        photoReview.setScore(photoReviewRequestDto.getScore());
        return photoReview;
    }

    public PhotoReview fromUpdateDto(int id, PhotoReviewRequestDto photoReviewRequestDto, ContestPhoto contestPhoto) {
        PhotoReview photoReview = photoReviewService.get(id, contestPhoto);
        photoReview.setWrongCategory(photoReviewRequestDto.isWrongCategory());
        photoReview.setComment(photoReviewRequestDto.getComment());
        photoReview.setScore(photoReviewRequestDto.getScore());
        return photoReview;
    }

    public PhotoReviewResponseDto toResponseDto(PhotoReview photoReview) {
        PhotoReviewResponseDto photoReviewResponseDto = new PhotoReviewResponseDto();
        photoReviewResponseDto.setContestId(photoReview.getContest().getId());
        photoReviewResponseDto.setContestPhotoId(photoReview.getContestPhoto().getId());
        photoReviewResponseDto.setJurorUsername(photoReview.getJuryMember().getUsername());
        photoReviewResponseDto.setComment(photoReview.getComment());
        photoReviewResponseDto.setScore(photoReview.getScore());
        return photoReviewResponseDto;
    }

    public List<PhotoReviewResponseDto> toResponseDtoList(List<PhotoReview> photoReviews) {
        List<PhotoReviewResponseDto> photoReviewResponseDto = new ArrayList<>();
        photoReviews.forEach(photoReview -> photoReviewResponseDto.add(toResponseDto(photoReview)));
        return photoReviewResponseDto;
    }

    public List<ContestPhotoAndPointsDto> toContestPhotoAndPointsDto(List<PhotoReview> photoReviews) {

        return photoReviews.stream()
                .collect(Collectors.groupingBy(
                        PhotoReview::getContestPhoto,
                        Collectors.averagingDouble(PhotoReview::getScore)
                ))
                .entrySet().stream()
                .map(entry -> new ContestPhotoAndPointsDto(entry.getKey(), entry.getValue()))
                .sorted(Comparator.comparing(dto -> dto.getContestPhoto().getCreatedAt()))
                .collect(Collectors.toList());
    }
}
