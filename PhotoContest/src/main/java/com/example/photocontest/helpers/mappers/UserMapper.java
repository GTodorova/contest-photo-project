package com.example.photocontest.helpers.mappers;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.RegisterDto;
import com.example.photocontest.models.dto.UpdateUserDto;
import com.example.photocontest.models.dto.UserRequestDto;
import com.example.photocontest.models.dto.UserResponseDto;
import com.example.photocontest.services.contracts.RankService;
import com.example.photocontest.services.contracts.RoleService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.photocontest.utils.UserRankConstants.INITIAL_RANK;
import static com.example.photocontest.utils.UserRoleConstants.PHOTO_JUNKIE;

@Component
public class UserMapper {

    public static final String PASSWORD_ERROR_MESSAGE = "Passwords do not match!";
    public static final String EMAIL_EXIST_ERROR_MESSAGE = "Email already exists!";
    private final UserService userService;
    private final RankService rankService;
    private final RoleService roleService;

    @Autowired
    public UserMapper(UserService userService, RankService rankService, RoleService roleService) {
        this.userService = userService;
        this.rankService = rankService;
        this.roleService = roleService;
    }

    private static void checkPasswordConfirm(RegisterDto registerDto) {
        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            throw new AuthenticationFailureException(PASSWORD_ERROR_MESSAGE);
        }
    }

    private static void checkPasswordConfirm(UpdateUserDto updateUserDto) {
        if (!updateUserDto.getPassword().equals(updateUserDto.getPasswordConfirm())) {
            throw new AuthenticationFailureException(PASSWORD_ERROR_MESSAGE);
        }
    }

    public User fromDto(int id, UserRequestDto userRequestDto) {
        User user = userService.get(id);
        user.setUsername(userRequestDto.getUsername());
        user.setFirstName(userRequestDto.getFirstName());
        user.setLastName(userRequestDto.getLastName());
        user.setPassword(userRequestDto.getPassword());
        user.setEmail(userRequestDto.getEmail());
        user.setCreatedAt(user.getCreatedAt());
        user.setRank(user.getRank());
        user.setRole(user.getRole());
        return user;
    }

    public User fromDtoUpdate(int id, UpdateUserDto dto) {
        User userToChange = userService.get(id);
        checkDuplicateEmailForUpdate(dto, userToChange);
        userToChange.setEmail(dto.getEmail());
        checkPasswordConfirm(dto);
        if (!dto.getPassword().equals("")) {
            userToChange.setPassword(dto.getPassword());
        }
        return userToChange;
    }

    public User fromRegisterDto(RegisterDto registerDto) {
        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        checkPasswordConfirm(registerDto);
        user.setPassword(registerDto.getPassword());
        checkDuplicateEmail(registerDto.getEmail());
        user.setEmail(registerDto.getEmail());
        user.setCreatedAt(LocalDateTime.now());
        user.setRank(rankService.get(INITIAL_RANK));
        user.setRole(roleService.get(PHOTO_JUNKIE));
        return user;
    }

    public UserResponseDto toResponseDto(User user) {
        UserResponseDto userDto = new UserResponseDto();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setRank(user.getRank().getName());
        userDto.setRole(user.getRole().getName());
        return userDto;
    }

    public UpdateUserDto toDtoUpdate(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();
        updateUserDto.setEmail(user.getEmail());
        updateUserDto.setPassword(user.getPassword());
        updateUserDto.setPasswordConfirm(user.getPassword());
        return updateUserDto;
    }

    public List<UserResponseDto> toResponseDto(List<User> users) {
        List<UserResponseDto> userResponseDto = new ArrayList<>();
        users.forEach(user -> userResponseDto.add(toResponseDto(user)));
        return userResponseDto;
    }

    private void checkDuplicateEmail(String email) {
        if (userService.findByEmail(email)) {
            throw new DuplicateEntityException(EMAIL_EXIST_ERROR_MESSAGE);
        }
    }

    private void checkDuplicateEmailForUpdate(UpdateUserDto dto, User user) {
        if (!dto.getEmail().equals(user.getEmail()) && userService.findByEmail(dto.getEmail())) {
            throw new DuplicateEntityException(EMAIL_EXIST_ERROR_MESSAGE);
        }
    }
}
