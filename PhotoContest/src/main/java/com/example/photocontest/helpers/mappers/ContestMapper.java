package com.example.photocontest.helpers.mappers;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestCreateDto;
import com.example.photocontest.models.dto.ContestOutDto;
import com.example.photocontest.models.dto.ContestUpdateDto;
import com.example.photocontest.services.contracts.CategoryService;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ContestMapper {

    public static final String REMAINING_FORMAT = "%d days, %d hours, %d minutes, and %d seconds remaining.";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private final ContestService contestService;

    private final CategoryService categoryService;

    private final UserService userService;

    @Autowired
    public ContestMapper(ContestService contestService, CategoryService categoryService, UserService userService) {
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.userService = userService;
    }

    public Contest fromDtoUpdate(int id, ContestUpdateDto dto) {
        Contest contestToChange = contestService.getById(id);
        contestToChange.setCoverPhotoName(dto.getCoverPhotoName());
        contestToChange.setTitle(dto.getTitle());
        return contestToChange;
    }

    public ContestUpdateDto toDtoUpdate(Contest contest) {
        ContestUpdateDto contestUpdateDto = new ContestUpdateDto();
        contestUpdateDto.setTitle(contest.getTitle());
        contestUpdateDto.setCoverPhotoName(contest.getCoverPhotoName());
        return contestUpdateDto;
    }

    public Contest fromDtoCreate(ContestCreateDto dto, User user) {
        Contest contest = new Contest();
        contest.setTitle(dto.getTitle());
        contest.setCategory(categoryService.getByCategoryName(dto.getCategoryName()));
        contest.setInvitational(dto.isInvitational());
        if (contest.isInvitational()) {
            Set<User> invitedUsers = new HashSet<>();
            for (String username : dto.getInvitedUsers()) {
                invitedUsers.add(userService.get(username));
            }
            contest.setInvitedUsers(invitedUsers);
        }
        contest.setPhaseOneStart(LocalDateTime.now());
        contest.setPhaseOneEnd(dto.getPhaseOneEnd());
        contest.setPhaseTwoEnd(dto.getPhaseTwoEnd());
        contest.setPhaseTwoStart(dto.getPhaseOneEnd());
        contest.setCreatedAt(LocalDateTime.now());
        contest.setCoverPhotoName(dto.getCoverPhotoName());
        Set<User> jury = new HashSet<>();
        for (String username : dto.getJuryMembers()) {
            jury.add(userService.get(username));
        }
        contest.setJuryMembers(jury);
        contest.setOrganizer(user);
        return contest;
    }

    public ContestOutDto contestToOutDto(Contest contest) {
        ContestOutDto contestOutDto = new ContestOutDto();
        contestOutDto.setId(contest.getId());
        contestOutDto.setTitle(contest.getTitle());
        contestOutDto.setCategoryName(contest.getCategory().getName());
        contestOutDto.setFinishedDate(changeTimeFormat(contest.getPhaseTwoEnd()));
        contestOutDto.setStartingDate(contest.getPhaseOneStart());
        contestOutDto.setPhaseTwoStart(changeTimeFormat(contest.getPhaseTwoStart()));
        contestOutDto.setCoverPhotoName(contest.getCoverPhotoName());
        contestOutDto.setInvitational(contest.isInvitational());
        Set<String> jury = new HashSet<>();
        for (User users : contest.getJuryMembers()) {
            jury.add(users.getUsername());
        }
        contestOutDto.setJuryFromMasters(jury);
        contestOutDto.setOrganizer(contest.getOrganizer().getUsername());
        Set<String> usernames = new HashSet<>();
        if (contest.isInvitational()) {
            for (User users : contest.getInvitedUsers()) {
                usernames.add(users.getUsername());
            }
            contestOutDto.setInvitedUsers(usernames);
        }
        LocalDateTime phaseOneStart = contest.getPhaseOneStart();
        LocalDateTime middleDate = contest.getPhaseOneEnd();
        LocalDateTime phaseTwoEnd = contest.getPhaseTwoEnd();
        contestOutDto.setCurrentPhase(contest.getCurrentPhase());

        if (LocalDateTime.now().isBefore(middleDate) &&
                LocalDateTime.now().isAfter(phaseOneStart)) {
            contestOutDto.setRemainingTime(checkRemainingTime(middleDate));
        } else if (LocalDateTime.now().isAfter(middleDate) && LocalDateTime.now().isBefore(phaseTwoEnd)) {
            contestOutDto.setRemainingTime(checkRemainingTime(phaseTwoEnd));
        } else {
            contestOutDto.setRemainingTime(checkRemainingTime(LocalDateTime.now()));
        }
        return contestOutDto;
    }

    public String checkRemainingTime(LocalDateTime endDate) {

        LocalDateTime currentDate = LocalDateTime.now();
        Duration duration = Duration.between(currentDate, endDate);

        Month currentMonth = LocalDateTime.now().getMonth();
        YearMonth yearMonthObject = YearMonth.of(currentDate.getYear(), currentMonth);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        long days = duration.toDays() % daysInMonth;
        long hours = duration.toHours() % 24;
        long minutes = duration.toMinutes() % 60;
        long seconds = duration.toSeconds() % 60;

        return String.format(REMAINING_FORMAT, days, hours, minutes, seconds);
    }

    public List<ContestOutDto> contestsToOutDto(List<Contest> contests) {
        List<ContestOutDto> contestOutDto = new ArrayList<>();
        for (Contest contest : contests) {
            contestOutDto.add(contestToOutDto(contest));
        }
        return contestOutDto;
    }

    public String changeTimeFormat(LocalDateTime time) {
        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
        return time.format(outputFormat);
    }
}
