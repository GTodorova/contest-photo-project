package com.example.photocontest.helpers.mappers;

import com.example.photocontest.models.Place;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;
import org.springframework.stereotype.Component;

@Component
public class WinnerPhotoMapper {

    public WinnerPhoto toWinnerPhoto(ContestPhotoAndPointsDto dto, int ratingPoints, Place place) {
        WinnerPhoto winnerPhoto = new WinnerPhoto();

        winnerPhoto.setContestPhoto(dto.getContestPhoto());
        winnerPhoto.setPlace(place);
        winnerPhoto.setRatingPoints(ratingPoints);
        return winnerPhoto;
    }
}
