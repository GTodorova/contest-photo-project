package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.User;
import com.example.photocontest.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class GlobalControllerAdvice {
    private final UserService userService;

    @Autowired
    public GlobalControllerAdvice(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserName")
    public String populateCurrentUserName(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("isOrganizer")
    public boolean isOrganizer(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return userService.isOrganizer(user);
        } catch (EntityNotFoundException e) {
            return false;
        }
    }
}
