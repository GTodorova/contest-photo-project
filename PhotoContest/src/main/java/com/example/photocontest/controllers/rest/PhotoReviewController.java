package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.PhotoReviewMapper;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.PhotoReviewRequestDto;
import com.example.photocontest.models.dto.PhotoReviewResponseDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.PhotoReviewService;
import com.example.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;

@RestController
@RequestMapping("/api/contest-photo/{contestPhotoId}/photo-review")
public class PhotoReviewController {

    public static final String ERROR_WRONG_PHOTO_REVIEW =
            "Photo review with id %d does not belong to contest photo with id %d";
    public static final String ERROR_MESSAGE = "You are not a juror for this contest";
    public static final String ACCESS_DENIED_MESSAGE = "You are not allowed to update this photo review";
    private final ContestPhotoService contestPhotoService;

    private final PhotoReviewService photoReviewService;

    private final UserService userService;

    private final PhotoReviewMapper photoReviewMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhotoReviewController(ContestPhotoService contestPhotoService, PhotoReviewService photoReviewService,
                                 PhotoReviewMapper photoReviewMapper, UserService userService,
                                 AuthenticationHelper authenticationHelper) {
        this.contestPhotoService = contestPhotoService;
        this.photoReviewService = photoReviewService;
        this.photoReviewMapper = photoReviewMapper;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping()
    public List<PhotoReviewResponseDto> get(@PathVariable Integer contestPhotoId) {
        try {
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            List<PhotoReview> photoReviews = photoReviewService.getAllByContestPhoto(contestPhoto);
            return photoReviewMapper.toResponseDtoList(photoReviews);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PhotoReviewResponseDto get(@PathVariable Integer contestPhotoId, @PathVariable Integer id) {
        try {
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            PhotoReview photoReview = photoReviewService.get(id, contestPhoto);
            if (photoReview.getContestPhoto().getId() != contestPhoto.getId()) {
                throw new EntityNotFoundException(format(ERROR_WRONG_PHOTO_REVIEW, id, contestPhotoId));
            }
            return photoReviewMapper.toResponseDto(photoReview);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public PhotoReviewResponseDto create(@RequestHeader HttpHeaders headers, @PathVariable Integer contestPhotoId,
                                         @RequestBody @Valid PhotoReviewRequestDto photoReviewRequestDto) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            checkAccessJuror(executingUser, contestPhoto);
            PhotoReview photoReview = photoReviewMapper.fromCreateDto(photoReviewRequestDto, executingUser);

            photoReviewService.create(photoReview, contestPhoto, executingUser);

            return photoReviewMapper.toResponseDto(photoReview);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public PhotoReviewResponseDto update(@RequestHeader HttpHeaders headers, @PathVariable Integer contestPhotoId,
                                         @PathVariable Integer id,
                                         @RequestBody @Valid PhotoReviewRequestDto photoReviewRequestDto) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            checkAccessJuror(executingUser, contestPhoto);
            PhotoReview photoReviewToUpdate = photoReviewMapper.fromUpdateDto(id, photoReviewRequestDto, contestPhoto);

            if (executingUser.getId() != photoReviewToUpdate.getJuryMember().getId()) {
                throw new UnauthorizedOperationException(ACCESS_DENIED_MESSAGE);
            }

            photoReviewService.update(photoReviewToUpdate);

            return photoReviewMapper.toResponseDto(photoReviewToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Integer contestPhotoId,
                       @PathVariable Integer id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            checkAccessJuror(user, contestPhoto);

            PhotoReview photoReview = photoReviewService.get(id, contestPhoto);
            if (photoReview.getContestPhoto().getId() != contestPhoto.getId()) {
                throw new EntityNotFoundException(format(ERROR_WRONG_PHOTO_REVIEW, id, contestPhotoId));
            }
            photoReviewService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    private void checkAccessJuror(User user, ContestPhoto contestPhoto) {
        if (!contestPhoto.getContest().getJuryMembers().contains(user) && !userService.isOrganizer(user)) {
            throw new UnauthorizedOperationException(ERROR_MESSAGE);
        }
    }
}
