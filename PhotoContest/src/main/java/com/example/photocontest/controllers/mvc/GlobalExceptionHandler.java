package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ModelAndView handleInternalServerError(Exception e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("NotFoundView");
        return modelAndView;
    }

    @ExceptionHandler(UnauthorizedOperationException.class)
    public ModelAndView handleUnauthorizedException(UnauthorizedOperationException e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UnauthorizedView");
        modelAndView.addObject("errorMessage", e.getMessage());
        return modelAndView;
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ModelAndView handleNotFoundException(EntityNotFoundException e) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("NotFoundView");
        modelAndView.addObject("errorMessage", e.getMessage());
        return modelAndView;
    }
}
