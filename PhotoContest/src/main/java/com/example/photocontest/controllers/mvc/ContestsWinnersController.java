package com.example.photocontest.controllers.mvc;

import com.example.photocontest.services.contracts.WinnerPhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/contestWinners")
public class ContestsWinnersController {
    private final WinnerPhotosService winnerPhotosService;

    @Autowired
    public ContestsWinnersController(WinnerPhotosService winnerPhotosService) {
        this.winnerPhotosService = winnerPhotosService;
    }

    @GetMapping
    public String getTotalWinners(Model model) {
        model.addAttribute("contestPhotosWinners", winnerPhotosService.getAllWinners());
        return "contestsPhotosWinners";
    }
}
