package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.UserMapper;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ForgotPasswordDto;
import com.example.photocontest.models.dto.LoginDto;
import com.example.photocontest.models.dto.RegisterDto;
import com.example.photocontest.models.dto.ResetPasswordDto;
import com.example.photocontest.services.contracts.UserService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    private final JavaMailSender mailSender;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationMvcController(UserService userService,
                                       AuthenticationHelper authenticationHelper,
                                       UserMapper userMapper, JavaMailSender mailSender) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.mailSender = mailSender;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "LoginView";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "LoginView";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            session.setAttribute("isOrganizer", userService.isOrganizer(user));


            if (userService.isOrganizer(user)) {
                return "redirect:/organizer-home";
            }
            return "redirect:/home";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "LoginView";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "RegisterView";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "RegisterView";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error",
                    "Password confirmation should match password.");
            return "RegisterView";
        }

        try {
            User user = userMapper.fromRegisterDto(register);
            userService.create(user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("username")) {
                bindingResult.rejectValue("username", "username_error", e.getMessage());
            } else {
                bindingResult.rejectValue("email", "email_error", e.getMessage());
            }
            return "RegisterView";
        }
    }

    @GetMapping("/forgot-password")
    public String showForgotPasswordPage(Model model) {
        model.addAttribute("forgotPassword", new ForgotPasswordDto());
        return "ForgotPasswordView";
    }

    @PostMapping("/forgot-password")
    public String handleForgotPassword(@Valid @ModelAttribute("forgotPassword") ForgotPasswordDto forgotPassword,
                                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ForgotPasswordView";
        }

        try {
            User user = userService.getUserByEmail(forgotPassword.getEmail());
            String token = UUID.randomUUID().toString();
            userService.updateResetPasswordToken(token, user.getEmail());
            String resetPasswordLink = "http://localhost:8080/auth/reset-password?token=" + token;
            sendEmail(user.getEmail(), resetPasswordLink);
            return "SendEmailMessage";
        } catch (UnauthorizedOperationException | MessagingException | UnsupportedEncodingException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "ForgotPasswordView";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "email_error", e.getMessage());
            return "ForgotPasswordView";
        }
    }


    @GetMapping("/reset-password")
    public String showResetPasswordPage(@Param(value = "token") String token, Model model) {
        try {
            User user = userService.getByResetPasswordToken(token);
            model.addAttribute("token", token);

            if (user == null) {
                model.addAttribute("message", "Invalid Token");
                return "ForgotPasswordView";
            }
            model.addAttribute("resetPassword", new ResetPasswordDto());
            return "ResetPasswordView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("message", e.getMessage());
            return "InvalidTokenMessage";
        }
    }

    @PostMapping("/reset-password")
    public String handleResetPassword(@Valid @ModelAttribute("resetPassword") ResetPasswordDto resetPassword,
                                      BindingResult bindingResult,
                                      @Param(value = "token") String token,
                                      Model model) {
        User user = userService.getByResetPasswordToken(token);
        model.addAttribute("title", "Reset your password");

        if (user == null) {
            model.addAttribute("message", "Invalid Token");
            return "InvalidTokenMessage";
        } else {
            if (bindingResult.hasErrors()) {
                model.addAttribute("token", token);
                return "ResetPasswordView";
            }

            if (!resetPassword.getPassword().equals(resetPassword.getPasswordConfirm())) {
                bindingResult.rejectValue("passwordConfirm", "password_error",
                        "Password confirmation should match password.");
                model.addAttribute("token", token);
                return "ResetPasswordView";
            }
            userService.updatePassword(user, resetPassword.getPassword());

            model.addAttribute("message", "You have successfully changed your password.");
        }
        return "ResetPasswordSuccessMessage";
    }

    private void sendEmail(String recipientEmail, String link) throws MessagingException, UnsupportedEncodingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("no-photo@contest.com", "Contest Support");
        helper.setTo(recipientEmail);

        String subject = "Here's the link to reset your password";

        String content = "<p>Hello,</p>"
                + "<p>You have requested to reset your password.</p>"
                + "<p>Click the link below to change your password:</p>"
                + "<p><a href=\"" + link + "\">Change my password</a></p>"
                + "<br>"
                + "<p>Ignore this email if you do remember your password, "
                + "or you have not made the request.</p>";

        helper.setSubject(subject);
        helper.setText(content, true);
        mailSender.send(message);
    }
}
