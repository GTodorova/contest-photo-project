package com.example.photocontest.controllers.mvc;

import com.example.photocontest.helpers.mappers.ContestMapper;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;
import com.example.photocontest.models.dto.UserFilterDto;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;

    private final ContestService contestService;

    private final ContestMapper contestMapper;


    public HomeMvcController(UserService userService, ContestService contestService, ContestMapper contestMapper) {
        this.userService = userService;
        this.contestService = contestService;
        this.contestMapper = contestMapper;
    }

    @GetMapping
    public String showHomePage(Model model) {
        model.addAttribute("contests", contestMapper.contestsToOutDto(contestService.getMoreRecentContests()).stream().limit(2).collect(Collectors.toList()));
        model.addAttribute("numberOfContests", contestService.getNumberOfContests());
        return "HomeView";
    }

    @GetMapping("/about")
    public String getAbout() {
        return "AboutView";
    }

    @GetMapping("/contacts")
    public String getContact() {
        return "ContactView";
    }

    @GetMapping("/organizer-home")
    public String getOrganizerHomePage(Model model) {
        model.addAttribute("contests",
                contestMapper.contestsToOutDto(contestService.getMoreRecentContests()));
        return "HomeOrganizerView";
    }

    @GetMapping("/home")
    public String getPhotoJunkieHomePage(Model model) {
        model.addAttribute("contests",
                contestMapper.contestsToOutDto(contestService.getMoreRecentContests()).stream().limit(4)
                        .collect(Collectors.toList()));
        return "HomePhotoJunkieView";
    }

    @GetMapping("/panel")
    public String showPanelWithAllUsers(@ModelAttribute("userFilerOptions") UserFilterDto userFilterDto, Model model) {
        UserFilterOptions userFilterOptions = new UserFilterOptions(
                userFilterDto.getUsername(),
                userFilterDto.getFirstName(),
                userFilterDto.getLastName(),
                userFilterDto.getSortBy(),
                userFilterDto.getSortOrder());
        List<User> users = userService.get(userFilterOptions);
        model.addAttribute("userFilterOptions", userFilterDto);
        model.addAttribute("users", users);
        return "PanelView";
    }
}
