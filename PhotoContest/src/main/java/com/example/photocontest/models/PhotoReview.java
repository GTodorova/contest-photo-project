package com.example.photocontest.models;


import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "photo_reviews")
public class PhotoReview {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_review_id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "contest_photo_id")
    private ContestPhoto contestPhoto;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User juryMember;

    @Column(name = "comment")
    private String comment;

    @Column(name = "is_wrong_category")
    private boolean isWrongCategory;

    @Column(name = "juro_points")
    private int score;

    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    public PhotoReview() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContestPhoto getContestPhoto() {
        return contestPhoto;
    }

    public void setContestPhoto(ContestPhoto contestPhoto) {
        this.contestPhoto = contestPhoto;
    }

    public User getJuryMember() {
        return juryMember;
    }

    public void setJuryMember(User juryMember) {
        this.juryMember = juryMember;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isWrongCategory() {
        return isWrongCategory;
    }

    public void setWrongCategory(boolean wrongCategory) {
        isWrongCategory = wrongCategory;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhotoReview that = (PhotoReview) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
