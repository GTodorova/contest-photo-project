package com.example.photocontest.models.dto;

import java.time.LocalDateTime;
import java.util.Set;

public class ContestOutDto {

    private int id;
    private String title;

    private String categoryName;

    private String currentPhase;

    private String remainingTime;

    private LocalDateTime startingDate;

    private String phaseTwoStart;

    private String finishedDate;

    private String coverPhotoName;

    private Boolean isInvitational;

    private Set<String> invitedUsers;

    private Set<String> juryFromMasters;


    private String organizer;

    public ContestOutDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<String> getJuryFromMasters() {
        return juryFromMasters;
    }

    public void setJuryFromMasters(Set<String> juryFromMasters) {
        this.juryFromMasters = juryFromMasters;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getCurrentPhase() {
        return currentPhase;
    }

    public void setCurrentPhase(String currentPhase) {
        this.currentPhase = currentPhase;
    }

    public String getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(String phaseTwoStart) {
        this.phaseTwoStart = phaseTwoStart;
    }

    public String getCoverPhotoName() {
        return coverPhotoName;
    }

    public void setCoverPhotoName(String coverPhotoName) {
        this.coverPhotoName = coverPhotoName;
    }

    public Boolean getInvitational() {
        return isInvitational;
    }

    public void setInvitational(Boolean invitational) {
        isInvitational = invitational;
    }

    public String getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(String finishedDate) {
        this.finishedDate = finishedDate;
    }

    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public Set<String> getInvitedUsers() {
        return invitedUsers;
    }

    public void setInvitedUsers(Set<String> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }

    public String getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(String remainingTime) {
        this.remainingTime = remainingTime;
    }
}
