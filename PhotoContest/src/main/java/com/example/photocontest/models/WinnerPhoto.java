package com.example.photocontest.models;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "winner_photos")
public class WinnerPhoto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "winner_photo_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "contest_photo_id")
    private ContestPhoto contestPhoto;

    @OneToOne
    @JoinColumn(name = "place_id")
    private Place place;

    @Column(name = "points_winners")
    private float points;

    public WinnerPhoto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContestPhoto getContestPhoto() {
        return contestPhoto;
    }

    public void setContestPhoto(ContestPhoto contestPhoto) {
        this.contestPhoto = contestPhoto;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public float getPoints() {
        return points;
    }

    public void setRatingPoints(int points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WinnerPhoto that = (WinnerPhoto) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
