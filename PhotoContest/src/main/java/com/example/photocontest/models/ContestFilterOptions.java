package com.example.photocontest.models;

import java.util.Optional;

public class ContestFilterOptions {

    private final Optional<String> title;
    private final Optional<Integer> categoryId;

    private final Optional<String> phase;
    private final Optional<String> sortBy;
    private final Optional<String> sortOrder;

    public ContestFilterOptions() {
        this(null, null, null, null, null);
    }

    public ContestFilterOptions(
            String title,
            Integer categoryId,
            String phase,
            String sortBy,
            String sortOrder) {
        this.title = Optional.ofNullable(title);
        this.categoryId = Optional.ofNullable(categoryId);
        this.phase = Optional.ofNullable(phase);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getTitle() {
        return title;
    }

    public Optional<Integer> getCategoryId() {
        return categoryId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getPhase() {
        return phase;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
