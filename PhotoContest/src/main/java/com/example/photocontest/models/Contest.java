package com.example.photocontest.models;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(name = "is_invitational")
    private boolean isInvitational;

    @Column(name = "phase_one_start")
    @CreationTimestamp
    private LocalDateTime phaseOneStart;

    @Column(name = "phase_one_end")
    private LocalDateTime phaseOneEnd;

    @Column(name = "phase_two_start")
    private LocalDateTime phaseTwoStart;

    @Column(name = "phase_two_end")
    private LocalDateTime phaseTwoEnd;

    @Column(name = "cover_photo_name")
    private String coverPhotoName;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User organizer;

    @Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "jury_members",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> juryMembers;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinTable(
            name = "invited_users",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> invitedUsers;

    public Contest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isInvitational() {
        return isInvitational;
    }

    public void setInvitational(boolean invitational) {
        isInvitational = invitational;
    }

    public Set<User> getJuryMembers() {
        return juryMembers;
    }

    public void setJuryMembers(Set<User> juryMembers) {
        this.juryMembers = juryMembers;
    }

    public LocalDateTime getPhaseOneStart() {
        return phaseOneStart;
    }

    public void setPhaseOneStart(LocalDateTime phaseOneStart) {
        this.phaseOneStart = phaseOneStart;
    }

    public LocalDateTime getPhaseOneEnd() {
        return phaseOneEnd;
    }

    public void setPhaseOneEnd(LocalDateTime phaseOneEnd) {
        this.phaseOneEnd = phaseOneEnd;
    }

    public Set<User> getInvitedUsers() {
        return invitedUsers;
    }

    public void setInvitedUsers(Set<User> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }

    public LocalDateTime getPhaseTwoStart() {
        return phaseTwoStart;
    }

    public void setPhaseTwoStart(LocalDateTime phaseTwoStart) {
        this.phaseTwoStart = phaseOneEnd;
    }

    public LocalDateTime getPhaseTwoEnd() {
        return phaseTwoEnd;
    }

    public void setPhaseTwoEnd(LocalDateTime phaseTwoEnd) {
        this.phaseTwoEnd = phaseTwoEnd;
    }

    public String getCoverPhotoName() {
        return coverPhotoName;
    }

    public void setCoverPhotoName(String coverPhotoName) {
        this.coverPhotoName = coverPhotoName;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getCurrentPhase() {

        if (LocalDateTime.now().isAfter(phaseOneStart)
                && LocalDateTime.now().isBefore(phaseOneEnd)) {
            return "First phase";
        } else if (LocalDateTime.now().isAfter(phaseTwoStart)
                && LocalDateTime.now().isBefore(phaseTwoEnd)) {
            return "Second phase";
        } else {
            return "Finished";
        }
    }

    public boolean isFirstPhase() {
        return phaseOneEnd.isAfter(LocalDateTime.now()) && phaseOneStart.isBefore(LocalDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return id == contest.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
