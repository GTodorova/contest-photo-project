package com.example.photocontest.models.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class UpdateUserDto {

    @NotEmpty(message = "Email can't be empty")
    @Pattern(regexp = "^[A-Za-z0-9+_.-]+@(.+)$", message = "Invalid email")
    private String email;

    @NotEmpty(message = "Password can't be empty")
    @Size(min = 8, message = "Password should be at least 8 symbols")
    private String password;

    @NotEmpty(message = "Confirm password confirmation can't be empty")
    @Size(min = 8, message = "Confirm password should be at least 8 symbols")
    private String passwordConfirm;

    public UpdateUserDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
