package com.example.photocontest.models.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.multipart.MultipartFile;


public class ContestPhotoInputDto {

    @NotNull(message = "File cannot be null")
    private MultipartFile file;

    @NotEmpty(message = "Photo title can't be empty")
    private String photoTitle;
    @NotEmpty(message = "Story can't be empty")
    private String story;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
