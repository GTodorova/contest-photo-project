package com.example.photocontest.exceptions;

public class InvalidInputOperation extends RuntimeException {
    public InvalidInputOperation(String message) {
        super(message);
    }
}
