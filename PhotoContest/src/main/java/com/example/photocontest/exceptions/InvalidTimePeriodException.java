package com.example.photocontest.exceptions;

public class InvalidTimePeriodException extends RuntimeException {
    public InvalidTimePeriodException(String message) {
        super(message);
    }
}
