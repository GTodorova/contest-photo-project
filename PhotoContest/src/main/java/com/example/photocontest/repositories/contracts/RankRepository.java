package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Rank;

import java.util.List;

public interface RankRepository {

    List<Rank> getAll();

    Rank get(int id);

    Rank get(String name);

    Rank getRankByScore(int score);
}
