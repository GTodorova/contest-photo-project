package com.example.photocontest.repositories;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.repositories.contracts.WinnerPhotosRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WinnerPhotosRepositoryImpl extends AbstractCRUDRepository<WinnerPhoto> implements WinnerPhotosRepository {
    SessionFactory sessionFactory;

    @Autowired
    public WinnerPhotosRepositoryImpl(SessionFactory sessionFactory) {
        super(WinnerPhoto.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestPhoto> getContestPhotosWinnersByContestId(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT wp FROM WinnerPhoto wp JOIN ContestPhoto cp ON wp.contestPhoto.id = cp.id WHERE cp.contest.id = :contestId";
            Query<ContestPhoto> query = session.createQuery(hql, ContestPhoto.class);
            query.setParameter("contestId", contestId);
            return query.list();
        }
    }

    @Override
    public List<WinnerPhoto> getByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT wp FROM WinnerPhoto wp JOIN ContestPhoto cp ON wp.contestPhoto.id = cp.id " +
                    "WHERE cp.user = :user ORDER BY wp.points DESC";
            Query<WinnerPhoto> query = session.createQuery(hql, WinnerPhoto.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public List<WinnerPhoto> getWinnerPhotosByContest(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "SELECT wp FROM WinnerPhoto wp " +
                    "JOIN ContestPhoto cp ON wp.contestPhoto.id = cp.id WHERE cp.contest = :contest " +
                    "ORDER BY wp.points DESC";
            Query<WinnerPhoto> query = session.createQuery(hql, WinnerPhoto.class);
            query.setParameter("contest", contest);
            return query.list();
        }
    }
}
