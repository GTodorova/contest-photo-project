package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Rank;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;
import com.example.photocontest.repositories.contracts.RankRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.photocontest.utils.UserRoleConstants.CAN_FOUND_ROLE_ORGANIZER;

@Repository
public class UserRepositoryImpl implements UserRepository {


    private final SessionFactory sessionFactory;
    private final RankRepository rankRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, RankRepository rankRepository) {
        this.sessionFactory = sessionFactory;
        this.rankRepository = rankRepository;
    }

    @Override
    public List<User> get(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            userFilterOptions.getUsername().ifPresent(value -> {
                filters.add("username like :username");
                params.put("username", String.format("%%%s%%", value));
            });

            userFilterOptions.getFirstName().ifPresent(value -> {
                filters.add("firstName like :firstName");
                params.put("firstName", String.format("%%%s%%", value));
            });

            userFilterOptions.getLastName().ifPresent(value -> {
                filters.add("lastName like :lastName");
                params.put("lastName", String.format("%%%s%%", value));
            });

            StringBuilder queryString = new StringBuilder("from User");
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }
            queryString.append(generateOrderBy(userFilterOptions));

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
            return query.list();
        }
    }

    @Override
    public User get(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User get(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.isEmpty() || !result.get(0).getUsername().equals(username)) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> getOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where role.name = :roleName", User.class);
            query.setParameter("roleName", "Organizer");

            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(CAN_FOUND_ROLE_ORGANIZER);
            }
            return result;
        }
    }

    @Override
    public List<User> getUsersByRank(User user, String rankName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery
                    ("from User where rank.name = :rankName and id != :userId", User.class);
            query.setParameter("rankName", rankName);
            query.setParameter("userId", user.getId());
            return query.list();
        }
    }

    @Override
    public User findByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("Email not found");
            }

            return result.get(0);
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where resetPasswordToken = :token", User.class);
            query.setParameter("token", token);


            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "resetPasswordToken", token);
            }

            return result.get(0);
        }
    }

    private String generateOrderBy(UserFilterOptions userFilterOptions) {
        String orderBy = " order by createdAt desc ";
        if (userFilterOptions.getSortBy().isEmpty()) {
            return orderBy;
        }

        switch (userFilterOptions.getSortBy().get()) {

            case "username":
                orderBy = " order by username ";
                break;
            case "firstName":
                orderBy = " order by firstName ";
                break;
            case "lastName":
                orderBy = " order by lastName ";
                break;
            default:
                return orderBy;
        }

        if (userFilterOptions.getSortOrder().isPresent() &&
                userFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy += " desc";
        }

        return orderBy;
    }

    public void updateUserRangAndScoreInDB(User user, int newScore) {
        user.setScore(newScore);
        Rank rank = rankRepository.getRankByScore(newScore);
        user.setRank(rank);
        update(user);
    }
}