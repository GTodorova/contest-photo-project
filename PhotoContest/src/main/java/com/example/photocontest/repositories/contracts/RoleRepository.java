package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Role;

import java.util.List;

public interface RoleRepository {

    List<Role> getAll();

    Role get(int id);

    Role get(String name);
}
