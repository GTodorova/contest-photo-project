package com.example.photocontest.repositories;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;
import com.example.photocontest.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> get(ContestFilterOptions contestFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            contestFilterOptions.getTitle().ifPresent(value -> {
                filters.add(" title like :title ");
                params.put("title", "%" + value + "%");
            });

            contestFilterOptions.getCategoryId().ifPresent(value -> {
                filters.add(" category.id = :categoryId ");
                params.put("categoryId", value);
            });

            LocalDateTime currentDateTime = LocalDateTime.now();

            contestFilterOptions.getPhase().ifPresent(phaseType -> {
                switch (phaseType) {
                    case "first_phase":
                        filters.add(" :currentDateTime <= phaseOneEnd ");
                        params.put("currentDateTime", currentDateTime);
                        break;
                    case "second_phase":
                        filters.add(" :currentDateTime >= phaseOneEnd AND :currentDateTime <= phaseTwoEnd ");
                        params.put("currentDateTime", currentDateTime);
                        break;
                    case "finished":
                        filters.add(" :currentDateTime > phaseTwoEnd ");
                        params.put("currentDateTime", currentDateTime);
                        break;
                }
            });

            StringBuilder queryString = new StringBuilder(" from Contest");
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }
            queryString.append(generateOrderBy(contestFilterOptions));

            Query<Contest> query = session.createQuery(queryString.toString(), Contest.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateOrderBy(ContestFilterOptions contestFilterOptions) {
        String orderBy = " order by createdAt desc";

        if (contestFilterOptions.getSortBy().isEmpty()) {
            return orderBy;
        }

        switch (contestFilterOptions.getSortBy().get()) {
            case "title":
                orderBy = " order by title";
                break;
            case "category":
                orderBy = " order by category.name";
                break;
            default:
                return orderBy;
        }

        if (contestFilterOptions.getSortOrder().isPresent() && contestFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy += " desc";
        }

        return orderBy;
    }


    public List<Contest> getContestPhaseOne(LocalDateTime currentDate) {
        currentDate = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "FROM Contest WHERE phaseOneStart <= :currentDate AND phaseOneEnd > :currentDate order by createdAt desc ",
                    Contest.class
            );
            query.setParameter("currentDate", currentDate);
            return query.getResultList();
        }
    }

    public List<Contest> getContestPhaseTwo(LocalDateTime currentDate) {
        currentDate = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "FROM Contest WHERE phaseOneEnd <= :currentDate AND phaseTwoEnd > :currentDate order by createdAt desc",
                    Contest.class
            );
            query.setParameter("currentDate", currentDate);
            return query.getResultList();
        }
    }

    public List<Contest> getContestPhaseFinished(LocalDateTime currentDate) {
        currentDate = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "FROM Contest WHERE phaseTwoEnd < :currentDate order by createdAt desc",
                    Contest.class
            );
            query.setParameter("currentDate", currentDate);
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> getMoreRecentContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "FROM Contest ORDER BY createdAt desc ",
                    Contest.class
            );
            return query.setMaxResults(6).list();
        }
    }

    @Override
    public List<Contest> getFinishedContestsWithoutWinners() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    "SELECT c FROM Contest c " +
                            "JOIN ContestPhoto cp " +
                            "WHERE cp.id NOT IN (SELECT wp.contestPhoto.id FROM WinnerPhoto wp) " +
                            "AND c.phaseTwoEnd > CURRENT_DATE",
                    Contest.class
            );

            return query.getResultList();
        }
    }

    @Override
    public long getNumberOfContests() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("select count( distinct c.id ) from Contest c ", Long.class).getSingleResult();
        }
    }
}

