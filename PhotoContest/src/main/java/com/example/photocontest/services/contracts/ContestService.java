package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;
import com.example.photocontest.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;

public interface ContestService {

    List<Contest> get(ContestFilterOptions contestFilterOptions);

    void create(Contest contest, User user);

    List<User> getContestJuryByContestId(int id);

    List<Contest> getMoreRecentContests();

    List<Contest> getAll();

    String checkContestPhaseType(Contest contest);

    long getNumberOfContests();


    List<Contest> getContestPhaseOne(LocalDateTime currentDate, User user);

    List<Contest> getContestPhaseTwo(LocalDateTime currentDate, User user);

    List<Contest> getContestPhaseFinished(LocalDateTime currentDate, User user);

    Contest getById(int id);

    void update(Contest contest, User user);

    void delete(int id, User user);

    void update(Contest contest, User user, MultipartFile file);

    void checkUserPermission(User user);
}