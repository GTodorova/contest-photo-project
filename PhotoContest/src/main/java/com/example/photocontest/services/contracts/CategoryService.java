package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Category;
import com.example.photocontest.models.User;

import java.util.List;

public interface CategoryService {

    Category getById(int id);

    Category getByCategoryName(String categoryName);

    boolean categoryExist(String categoryName);

    List<Category> getAll();

    void delete(int id, User user);

    void create(Category category, User user);

    void update(Category category, User user);
}
