package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import com.example.photocontest.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    public static final String CREATE_CONTEST_ERROR_MESSAGE = "Only organizers can create contests";
    public static final String NAME = "name";
    public static final String CATEGORY = "Category";
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    public Category getByCategoryName(String categoryName) {
        return categoryRepository.getByField("name", categoryName);
    }

    public boolean categoryExist(String categoryName) {
        for (Category category : getAll()) {
            if (category.getName().equals(categoryName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public void delete(int id, User user) {
        checkOrganizerPermission(user);
        categoryRepository.delete(id);
    }

    @Override
    public void create(Category category, User user) {
        checkOrganizerPermission(user);
        checkForDuplicates(category);
        categoryRepository.create(category);
    }

    @Override
    public void update(Category category, User user) {
        checkOrganizerPermission(user);
        checkForDuplicates(category);
        categoryRepository.update(category);
    }

    public void checkOrganizerPermission(User user) {
        if (user.getRole().getId() != 1) {
            throw new UnauthorizedOperationException(CREATE_CONTEST_ERROR_MESSAGE);
        }
    }

    public void checkForDuplicates(Category category) {
        boolean duplicateExists = true;
        try {
            categoryRepository.getByField(NAME, category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(CATEGORY, NAME, category.getName());
        }
    }
}
