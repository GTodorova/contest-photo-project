package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.services.contracts.ImageFilesOperations;
import com.example.photocontest.utils.FileConstants;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageFilesOperationsImpl implements ImageFilesOperations {
    public static final String INVALID_FILE_EXTENSION_MESSAGE = "Invalid file extension. Only JPG, JPEG and PNG files are allowed.";
    public static final String INVALID_FILE_SIZE_MESSAGE = "File size limit exceeded. Max size is 5MB.";
    public static final String FAILED_TO_DELETE_FILE = "Failed to delete file";
    public static final String FILE_NOT_FOUND = "File not found";


    public void uploadImage(MultipartFile file, String photoName) throws IOException {
        Path directoryPath = Paths.get(FileConstants.PATH_TO_FILE);
        if (!Files.exists(directoryPath)) {
            Files.createDirectory(directoryPath);
        }

        String originalFilename = file.getOriginalFilename();
        String fileExtension = "";
        if (originalFilename != null) {
            int dotIndex = originalFilename.lastIndexOf('.');
            if (dotIndex > 0) {
                fileExtension = originalFilename.substring(dotIndex + 1);
            }
        }

        if (!fileExtension.equalsIgnoreCase("jpg") && !fileExtension.equalsIgnoreCase("jpeg") && !fileExtension.equalsIgnoreCase("png")) {
            throw new IllegalArgumentException(INVALID_FILE_EXTENSION_MESSAGE);
        }

        long fileSize = file.getSize();
        long fileMaxSize = 5 * 1024 * 1024; // 5MB limit
        if (fileSize > fileMaxSize) {
            throw new IllegalArgumentException(INVALID_FILE_SIZE_MESSAGE);
        }

        String newFilename = photoName.replace(FileConstants.PATH_TO_FILE, "");
        Path filePath = directoryPath.resolve(newFilename);
        try (OutputStream outputStream = Files.newOutputStream(filePath)) {
            outputStream.write(file.getBytes());
        }
    }

    public void deleteImage(String photoUrl) {
        Path imagePath = Paths.get(photoUrl);
        if (Files.exists(imagePath)) {
            try (var fileStream = Files.newOutputStream(imagePath)) {
                Files.delete(imagePath);
            } catch (IOException e) {
                throw new RuntimeException(FAILED_TO_DELETE_FILE);
            }

        } else {
            throw new EntityNotFoundException(FILE_NOT_FOUND);
        }
    }
}
