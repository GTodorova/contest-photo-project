package com.example.photocontest.services.contracts;

import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;

import java.util.List;

public interface UserService {
    List<User> get(UserFilterOptions userFilterOptions);

    List<User> getAll();

    User get(int id);

    User get(String username);

    void create(User user);

    void delete(int id, User user);

    boolean isOrganizer(User user);

    void update(int id, User user);

    List<User> getUsersByRank(User user, String rankName);

    boolean findByEmail(String email);

    User getUserByUsername(String username);

    void updateResetPasswordToken(String token, String email);

    User getByResetPasswordToken(String token);

    void updatePassword(User user, String newPassword);

    User getUserByEmail(String email);

    void changeRoleOfUser(User user, int userId);
}
