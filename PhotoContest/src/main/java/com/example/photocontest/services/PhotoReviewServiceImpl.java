package com.example.photocontest.services;

import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.PhotoReviewRepository;
import com.example.photocontest.services.contracts.PhotoReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.photocontest.utils.UserRoleConstants.ORGANIZER;

@Service
public class PhotoReviewServiceImpl implements PhotoReviewService {

    public static final String GIVE_REVIEW_ERROR_MESSAGE = "You are not part of the contest";
    public static final String WRONG_CATEGORY_MESSAGE = "The photo is not in the correct category";
    public static final String HAS_ALREADY_REVIEWED = "You have already reviewed this photo";
    public static final int ZERO = 0;
    public static final String CONTEST_PHASE_ERROR_MESSAGE = "You can not review pictures if the phase is not Second phase";
    public static final String SECOND_PHASE = "Second phase";
    public static final String WRONG_CATEGORY_NO_MORE_REVIEWS_MESSAGE = "You cannot give ratings to a photo in the wrong category.";
    private final PhotoReviewRepository photoReviewRepository;

    @Autowired
    public PhotoReviewServiceImpl(PhotoReviewRepository photoReviewRepository) {
        this.photoReviewRepository = photoReviewRepository;
    }


    @Override
    public List<PhotoReview> getAllByContestPhoto(ContestPhoto contestPhoto) {
        return photoReviewRepository.getAllByContestPhoto(contestPhoto);
    }

    @Override
    public PhotoReview get(int id, ContestPhoto contestPhoto) {
        return photoReviewRepository.get(id, contestPhoto);
    }

    @Override
    public void create(PhotoReview photoReview, ContestPhoto contestPhoto, User juror) {
        checkJurorAccess(juror, contestPhoto);
        checkPhaseOfContest(contestPhoto);
        checkForSecondReview(contestPhoto, juror);
        checkForWrongCategoryReview(contestPhoto);
        changeReviewsIfWrongCategory(photoReview, contestPhoto);
        photoReview.setContestPhoto(contestPhoto);
        photoReview.setJuryMember(juror);
        photoReviewRepository.create(photoReview);
    }

    private void changeReviewsIfWrongCategory(PhotoReview photoReview, ContestPhoto contestPhoto) {
        if (photoReview.getScore() == ZERO || photoReview.isWrongCategory()) {
            photoReview.setScore(ZERO);
            photoReview.setWrongCategory(true);
            photoReview.setComment(WRONG_CATEGORY_MESSAGE);
            for (PhotoReview photoReview1 : photoReviewRepository.getAllByContestPhoto(contestPhoto)) {
                photoReview1.setComment(WRONG_CATEGORY_MESSAGE);
                photoReview1.setScore(ZERO);
                photoReview1.setWrongCategory(true);
                photoReview1.setContestPhoto(contestPhoto);
                photoReviewRepository.update(photoReview1);
            }
        }
    }

    private void checkForWrongCategoryReview(ContestPhoto contestPhoto) {
        if (photoReviewRepository.getAllByContestPhoto(contestPhoto)
                .stream()
                .anyMatch(PhotoReview::isWrongCategory)) {
            throw new UnauthorizedOperationException(WRONG_CATEGORY_NO_MORE_REVIEWS_MESSAGE);
        }
    }

    private void checkPhaseOfContest(ContestPhoto contestPhoto) {
        if (!contestPhoto.getContest().getCurrentPhase().equals(SECOND_PHASE)) {
            throw new UnsupportedOperationException(CONTEST_PHASE_ERROR_MESSAGE);
        }
    }

    @Override
    public void update(PhotoReview photoReview) {
        photoReviewRepository.update(photoReview);
    }

    @Override
    public void delete(int id) {
        photoReviewRepository.delete(id);
    }

    @Override
    public List<PhotoReview> getAllByContest(Contest contest) {
        return photoReviewRepository.getByContest(contest);
    }

    private void checkJurorAccess(User juror, ContestPhoto contestPhoto) {
        if (!contestPhoto.getContest().getJuryMembers().contains(juror) && !juror.getRole().getName().equals(ORGANIZER)) {
            throw new UnauthorizedOperationException(GIVE_REVIEW_ERROR_MESSAGE);
        }
    }

    private void checkForSecondReview(ContestPhoto contestPhoto, User juror) {
        for (PhotoReview photoReview : getAllByContestPhoto(contestPhoto)) {
            if (photoReview.getJuryMember().getUsername().equals(juror.getUsername())) {
                throw new UnauthorizedOperationException(HAS_ALREADY_REVIEWED);
            }

        }
    }
}

