package com.example.photocontest.services;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.PasswordEncoder;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;
import com.example.photocontest.repositories.contracts.ContestPhotosRepository;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.RoleRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.photocontest.utils.UserRoleConstants.ORGANIZER;
import static com.example.photocontest.utils.UserRoleConstants.PHOTO_JUNKIE;

@Service
public class UserServiceImpl implements UserService {

    public static final String GET_USERS_ERROR_MESSAGE = "Only organizers can view users by rank.";
    public static final String ORGANIZER_CHANGE_ROLE_ERROR = "Only organizers can change the role of user";
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only authenticated user can modify his own profile.";
    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final ContestRepository contestRepository;
    private final ContestPhotosRepository contestPhotosRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
                           ContestRepository contestRepository,
                           ContestPhotosRepository contestPhotosRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.contestRepository = contestRepository;
        this.contestPhotosRepository = contestPhotosRepository;
    }

    @Override
    public List<User> get(UserFilterOptions userFilterOptions) {
        return userRepository.get(userFilterOptions)
                .stream()
                .filter(user -> !user.getInactive() && !user.getUsername().equals("Deleted user"))
                .toList();

    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User get(int id) {
        return userRepository.get(id);
    }

    @Override
    public User get(String username) {
        return userRepository.get(username);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.get(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        user.setPassword(PasswordEncoder.encode(user.getPassword()));
        userRepository.create(user);
    }

    @Override
    public void update(int id, User user) {
        checkPermissionsForDeleteOrUpdate(id, user);

        boolean duplicateExists = true;
        try {
            User existingUser = userRepository.get(user.getUsername());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        user.setPassword(PasswordEncoder.encode(user.getPassword()));
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        User deletedUser = get(1);
        checkPermissionsForDeleteOrUpdate(id, user);
        User userToDelete = userRepository.get(id);
        userToDelete.setInactive(true);
        if (isOrganizer(userToDelete)) {
            changeContestToDeletedUser(deletedUser, id);
        } else {
            changeContestPhotoToDeletedUser(deletedUser, id);
        }
        userRepository.update(userToDelete);
    }

    @Override
    public void changeRoleOfUser(User user, int userId) {
        if (!isOrganizer(user)) {
            throw new UnauthorizedOperationException(ORGANIZER_CHANGE_ROLE_ERROR);
        }
        User userToUpdate = userRepository.get(userId);
        if (userToUpdate.getRole().getName().equals(PHOTO_JUNKIE)) {
            userToUpdate.setRole(roleRepository.get(ORGANIZER));
        } else {
            userToUpdate.setRole(roleRepository.get(PHOTO_JUNKIE));
        }
        userRepository.update(userToUpdate);
    }

    @Override
    public boolean isOrganizer(User user) {
        if (user != null) {
            return user.getRole().getName().equals(ORGANIZER);
        }
        return false;
    }

    @Override
    public List<User> getUsersByRank(User user, String rankName) {
        if (isOrganizer(user)) {
            return userRepository.getUsersByRank(user, rankName);
        } else {
            throw new UnauthorizedOperationException(GET_USERS_ERROR_MESSAGE);
        }
    }


    @Override
    public boolean findByEmail(String email) {
        boolean duplicateExists = true;
        try {
            userRepository.findByEmail(email);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Email already exists");
        }
        return false;
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {
            throw new EntityNotFoundException("Could not find any customer with the email " + email);
        }
    }

    @Override
    public User getByResetPasswordToken(String token) {
        return userRepository.findByResetPasswordToken(token);
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        user.setPassword(newPassword);
        user.setResetPasswordToken(null);
        //password hashing
        user.setPassword(PasswordEncoder.encode(user.getPassword()));
        userRepository.update(user);
    }

    @Override
    public User getUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user != null) {
            return user;
        } else {
            throw new EntityNotFoundException("No email found");
        }
    }

    private void changeContestToDeletedUser(User deletedUser, int id) {
        contestRepository.getAll().stream()
                .filter(contest -> contest.getOrganizer().getId() == id)
                .forEach(contest -> {
                    if (contest.getOrganizer().getId() == id) {
                        contest.setOrganizer(deletedUser);
                        contestRepository.update(contest);
                    }
                });
    }

    private void changeContestPhotoToDeletedUser(User deletedUser, int id) {
        contestPhotosRepository.getAll().stream()
                .filter(contestPhoto -> contestPhoto.getUser().getId() == id)
                .forEach(contestPhoto -> {
                    if (contestPhoto.getUser().getId() == id) {
                        contestPhoto.setUser(deletedUser);
                        contestPhotosRepository.update(contestPhoto);
                    }
                });
    }

    private void checkPermissionsForDeleteOrUpdate(int userId, User user) {
        if (userId != user.getId() && !isOrganizer(user)) {
            throw new AuthenticationFailureException(MODIFY_USER_ERROR_MESSAGE);
        }
    }
}