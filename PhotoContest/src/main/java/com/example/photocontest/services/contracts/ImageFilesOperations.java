package com.example.photocontest.services.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageFilesOperations {
    void uploadImage(MultipartFile file, String photoUrl) throws IOException;

    void deleteImage(String photoUrl);
}
