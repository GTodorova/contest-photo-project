package com.example.photocontest.services;

import com.example.photocontest.exceptions.*;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.ContestPhotosRepository;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.ContestService;
import com.example.photocontest.services.contracts.ImageFilesOperations;
import com.example.photocontest.utils.FileConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class ContestServiceImpl implements ContestService {

    public static final String UPLOAD_PICTURE_ERROR_MESSAGE = "The picture you want to set should starts with 'https://' and ends with 'jpg', 'png' or 'jpeg'!";
    public static final String INVITE_JURY_ERROR_MESSAGE = "Only masters can be jury";
    public static final String INVITE_JUNKIES_ERROR_MESSAGE = "You can not invite juries";
    public static final String INVITE_ORGANIZERS_ERROR_MESSAGE = "You can not invite organizers";
    public static final String CONTEST_PHOTOS_CAN_NOT_BE_DELETED = "Contests with contest photos can not be deleted.";
    public static final String UNABLE_TO_UPLOAD_FILE = "Unable to upload file";
    public static final String ONLY_ORGANIZERS_CAN_CREATE_CONTESTS = "Only organizers can create contests";
    public static final String ONLY_OWNERS_CAN_MODIFY_OR_DELETE_CONTESTS = "Only owners can modify or delete contests";
    public static final String FIRST_PHASE_ERROR_MESSAGE = "First phase should be between one day and one month";
    public static final String SECOND_PHASE_ERROR_MESSAGE = "Second phase should be between one hour and one day";
    public static final String FIRST = "First";
    public static final String SECOND = "Second";
    public static final String FINISHED = "Finished";
    private final ContestRepository contestRepository;

    private final ContestPhotosRepository contestPhotosRepository;

    private final UserRepository userRepository;
    private final ImageFilesOperations imageFilesOperations;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, ContestPhotosRepository contestPhotosRepository,
                              UserRepository userRepository, ImageFilesOperations imageFilesOperations) {
        this.contestRepository = contestRepository;
        this.contestPhotosRepository = contestPhotosRepository;
        this.userRepository = userRepository;
        this.imageFilesOperations = imageFilesOperations;
    }

    private static void checkCoverPhotoStart(Contest contest) {
        if (!contest.getCoverPhotoName().startsWith("https://")) {
            throw new InvalidUploadException(UPLOAD_PICTURE_ERROR_MESSAGE);
        }
    }

    @Override
    public List<Contest> get(ContestFilterOptions contestFilterOptions) {
        return contestRepository.get(contestFilterOptions);
    }

    @Override
    public void create(Contest contest, User user) {
        checkPhasesOneDuration(contest);
        checkPhasesTwoDuration(contest);
        checkOrganizerPermission(user);
        checkJuryMembersRank(contest);
        checkInvitedForJury(contest);
        checkInvitedForOrganizers(contest);
        checkCoverPhotoStart(contest);
        checkCoverPhotoEnd(contest);

        boolean duplicateExists = true;
        try {
            contestRepository.getByField("title", contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contestRepository.create(contest);
    }

    private void checkCoverPhotoEnd(Contest contest) {
        if (!contest.getCoverPhotoName().endsWith("png")
                && !contest.getCoverPhotoName().endsWith("jpg") && !contest.getCoverPhotoName().endsWith("jpeg")) {
            throw new InvalidUploadException(UPLOAD_PICTURE_ERROR_MESSAGE);
        }
    }

    public void checkInvitedForOrganizers(Contest contest) {
        List<User> organizers = userRepository.getOrganizers();
        if (contest.isInvitational() && checkIfOrganizerIsInvited(new HashSet<>(organizers), contest.getInvitedUsers())) {
            throw new UnsupportedOperationException(INVITE_ORGANIZERS_ERROR_MESSAGE);
        }
    }

    public void checkInvitedForJury(Contest contest) {
        if (contest.isInvitational() && checkIfJuryIsInvited(contest.getJuryMembers(), contest.getInvitedUsers())) {
            throw new UnsupportedOperationException(INVITE_JUNKIES_ERROR_MESSAGE);
        }
    }

    public void checkJuryMembersRank(Contest contest) {
        for (User user1 : contest.getJuryMembers()) {
            if (!user1.getRank().getName().equals("Master")) {
                throw new UnauthorizedOperationException(INVITE_JURY_ERROR_MESSAGE);
            }
        }
    }


    @Override
    public List<User> getContestJuryByContestId(int id) {
        Contest contest = getById(id);
        List<User> jury = new ArrayList<>(contest.getJuryMembers());
        List<User> organizers = userRepository.getOrganizers();
        for (User organizer : organizers) {
            if (contest.getOrganizer().getId() != organizer.getId()) {
                jury.add(organizer);
            }
        }
        return jury;
    }

    void checkPhasesOneDuration(Contest contest) {
        LocalDateTime minDateForPhaseOne = contest.getPhaseOneStart().plusDays(1);
        LocalDateTime maxDateForPhaseOne = contest.getPhaseOneStart().plusMonths(1);
        if (contest.getPhaseOneEnd().isBefore(minDateForPhaseOne) || contest.getPhaseOneEnd().isAfter(maxDateForPhaseOne)) {
            throw new InvalidInputOperation(FIRST_PHASE_ERROR_MESSAGE);
        }
    }

    void checkPhasesTwoDuration(Contest contest) {
        LocalDateTime minDateForPhaseTwo = contest.getPhaseOneEnd().plusHours(1);
        LocalDateTime maxDateForPhaseTwo = contest.getPhaseOneEnd().plusHours(24);
        if (contest.getPhaseTwoEnd().isBefore(minDateForPhaseTwo) || contest.getPhaseTwoEnd().isAfter(maxDateForPhaseTwo)) {
            throw new InvalidInputOperation(SECOND_PHASE_ERROR_MESSAGE);
        }
    }

    @Override
    public String checkContestPhaseType(Contest contest) {

        if (contest.getPhaseOneEnd().isAfter(LocalDateTime.now())
                && contest.getPhaseOneStart().isBefore(LocalDateTime.now())) {
            return FIRST;
        } else if (contest.getPhaseTwoStart().isBefore(LocalDateTime.now())
                && contest.getPhaseTwoEnd().isAfter(LocalDateTime.now())) {
            return SECOND;
        } else {
            return FINISHED;
        }
    }

    @Override
    public long getNumberOfContests() {
        return contestRepository.getNumberOfContests();
    }


    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public List<Contest> getContestPhaseOne(LocalDateTime currentDate, User user) {
        checkOrganizerPermission(user);
        return contestRepository.getContestPhaseOne(currentDate);
    }

    @Override
    public List<Contest> getContestPhaseTwo(LocalDateTime currentDate, User user) {
        checkOrganizerPermission(user);
        return contestRepository.getContestPhaseTwo(currentDate);
    }

    @Override
    public List<Contest> getContestPhaseFinished(LocalDateTime currentDate, User user) {
        checkOrganizerPermission(user);
        return contestRepository.getContestPhaseFinished(currentDate);
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    public List<Contest> getMoreRecentContests() {
        return contestRepository.getMoreRecentContests();
    }


    @Override
    public void update(Contest contest, User user) {
        checkOrganizerAndOwnerPermission(contest.getId(), user);
        checkPhasesOneDuration(contest);
        checkPhasesTwoDuration(contest);

        Contest contestRepo;
        boolean duplicateExists = true;
        try {
            contestRepo = contestRepository.getByField("title", contest.getTitle());
            if (!contest.getCoverPhotoName().equals(contestRepo.getCoverPhotoName())) {
                deleteImageIfNotContestPhoto(contestRepo);
            }
            if (contestRepo.getId() == contest.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }

        contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User user) {
        checkOrganizerAndOwnerPermission(id, user);
        Contest contest = getById(id);
        if (contest.getJuryMembers() != null) {
            contest.getJuryMembers().clear();
            contestRepository.update(contest);
        }
        if (contest.getInvitedUsers() != null) {
            contest.getInvitedUsers().clear();
            contestRepository.update(contest);
        }
        for (ContestPhoto contestPhoto : contestPhotosRepository.getAll()) {
            if (contestPhoto.getContest().getId() == id) {
                throw new UnsupportedOperationException(CONTEST_PHOTOS_CAN_NOT_BE_DELETED);
            }
        }
        deleteImageIfNotContestPhoto(contest);
        contestRepository.delete(id);
    }

    @Override
    public void update(Contest contest, User user, MultipartFile file) {
        checkOrganizerPermission(user);
        deleteImageIfNotContestPhoto(contest);

        String imageName = UUID.randomUUID() + file.getOriginalFilename();
        String oldImageName = contest.getCoverPhotoName();
        contest.setCoverPhotoName(FileConstants.PATH_TO_FILE + imageName);
        contestRepository.update(contest);

        try {
            imageFilesOperations.uploadImage(file, contest.getCoverPhotoName());
        } catch (IOException e) {
            contest.setCoverPhotoName(oldImageName);
            contestRepository.update(contest);
            throw new FileUploadException(UNABLE_TO_UPLOAD_FILE);
        }
    }

    @Override
    public void checkUserPermission(User user) {
        if (!user.getRole().getName().equals("Organizer")) {
            throw new UnauthorizedOperationException(ONLY_ORGANIZERS_CAN_CREATE_CONTESTS);
        }
    }

    private void deleteImageIfNotContestPhoto(Contest contest) {
        String currentCoverName = contest.getCoverPhotoName();
        try {
            contestPhotosRepository.getByField("photoUrl", currentCoverName);
        } catch (EntityNotFoundException e) {
            if (!currentCoverName.startsWith("https://")) {
                imageFilesOperations.deleteImage(currentCoverName);
            }
        }
    }

    public void checkOrganizerPermission(User user) {
        if (user.getRole().getId() != 1) {
            throw new UnauthorizedOperationException(ONLY_ORGANIZERS_CAN_CREATE_CONTESTS);
        }
    }

    public void checkOrganizerAndOwnerPermission(int id, User user) {
        if (!user.getUsername().equals(getById(id).getOrganizer().getUsername())) {
            throw new UnauthorizedOperationException(ONLY_OWNERS_CAN_MODIFY_OR_DELETE_CONTESTS);
        }
    }

    public boolean checkIfJuryIsInvited(Set<User> juries, Set<User> invitedUsers) {
        for (User jury : juries) {
            if (invitedUsers.contains(jury)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkIfOrganizerIsInvited(Set<User> organizers, Set<User> invitedUsers) {
        for (User organizer : organizers) {
            if (invitedUsers.contains(organizer)) {
                return true;
            }
        }
        return false;
    }
}